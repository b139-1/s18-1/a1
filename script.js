console.log("A WILD POKEMON APPEARED!");

let trainer = {
	name: "John Wick",
	age: 23,
	pokemon: ["Lugia", "Rayquaza", "Kyogre", "Groudon"],
	friends: {
		hoenn: ["Tony Stark", "Peter Parker"],
		kanto: ["Thor Odinson", "Nick Fury"]
	},
	talk: function(ball) {
		if(ball > (this.pokemon.length - 1)) {
			console.warn("No Pokemon in bag!");
		} else {
			console.log(`${this.pokemon[ball]}, I choose you!`)
		}
	}
} 
let pokemon = {
	name: "",
	level: 0,
	health: 0,
	attack: 0,
	tackle: function() {
		console.log(`pokemon tackled pokemon and dealt pokemon${damageDealt} damage points!`);
		console.log(`pokemon's health reduced to ${damageTaken} points!`);
	},
	fainted: function() {
		console.warn(`pokemon has fainted!`);
	}
}

function pokemonster(pokeName, lvl) {
	this.name = pokeName;
	this.level = lvl;
	this.health = lvl * 4;
	this.attack = lvl * 2;
	let damageDealt = this.attack / 2;
	let loser = this.health <= 0;
	this.tackle = function(target) {
		let damageTaken = target.health - damageDealt
		console.log(`${this.name} tackled ${target.name} and dealt ${damageDealt} damage points!`);
		console.log(`${target.name}'s health reduced to ${damageTaken} points!`);
		if(target.health <= 0) {
			target.fainted();
		} else {
			target.health = damageTaken;
		}
	}
	this.fainted = function() {
		console.warn(`${this.name} has fainted!`);
	}
}
let diox = new pokemonster("Dioxys", 100);
let rayq = new pokemonster("Rayquaza", 67);
let kyog = new pokemonster("Kyogre", 65);
let grou = new pokemonster("Groudon", 69);

console.log(trainer);
console.log("Result of dot notation.");
console.log(trainer.name);
console.log("Result of bracket notation.");
console.log(trainer.pokemon);
console.log("Result of talk method.");
console.log(trainer.talk(2));
console.log(diox);
console.log(rayq);
console.log(kyog);
console.log(grou);
console.log(`A wild ${diox.name} appeared!`);
console.log(trainer.talk(1));
console.log(rayq.tackle(diox));
console.log(diox);
console.log(rayq);
console.log(diox.tackle(rayq));
console.log(diox);
console.log(rayq);
console.log(rayq.tackle(diox));
console.log(diox);
console.log(rayq);
console.log(diox.tackle(rayq));
console.log(diox);
console.log(rayq);
console.log(rayq.tackle(diox));
console.log(diox);
console.log(rayq);
console.log(diox.tackle(rayq));
console.log(diox);
console.log(rayq);